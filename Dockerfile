FROM maven:3.6.3-jdk-11
COPY . /source
WORKDIR /source
RUN mvn clean package
ENTRYPOINT ["java", "-jar", "target/adsb-antenna-feeder-0.1.jar"]