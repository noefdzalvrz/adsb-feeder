package adsb.antenna.feeder

import io.micronaut.rabbitmq.annotation.Binding
import io.micronaut.rabbitmq.annotation.RabbitClient

@RabbitClient
interface OpenSkyFeederQueue {

    @Binding("opensky")
    fun send(data: ByteArray)
}
