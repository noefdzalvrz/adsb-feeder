package adsb.antenna.feeder

import com.google.gson.Gson
import io.micronaut.configuration.picocli.PicocliRunner
import io.micronaut.scheduling.annotation.Scheduled
import kong.unirest.Unirest
import kong.unirest.json.JSONArray

import picocli.CommandLine.Command
import picocli.CommandLine.Option
import javax.inject.Inject

@Command(name = "adsb-antenna-feeder", description = ["..."], mixinStandardHelpOptions = true)
class AdsbAntennaFeederCommand @Inject constructor(
    val rawAdbsAntennaClient: RawAdsbClient,
    val parsedAdsbClient: ParsedAdsbClient,
    val openSkyFeederQueue: OpenSkyFeederQueue
) : Runnable {

    @Option(names = ["-v", "--verbose"], description = ["..."])
    private var verbose: Boolean = false

    @Option(names = ["--host"], required = true, description = ["antenna host"])
    private var host: String = ""

    @Option(names = ["-p"], required = true, description = ["antenna port"])
    private var port: Int = 0

    @Option(names = ["-m"], required = true, description = ["RAW", "PARSED"])
    private var mode: String = "RAW"

    override fun run() {
        if (verbose)
            println("Connecting to $host:$port")
        val client = RawAdbsAntennaClient(host, port)
        val send = { data:String -> when(mode) {
            "PARSED" -> parsedAdsbClient.send(data.toByteArray())
            "RAW" -> rawAdbsAntennaClient.send(data.toByteArray())
        } }
        client.onNewMessage { send(it); println("Got message $it") }
        while (true){
            Thread.sleep(1000)
        }
    }

    @Scheduled(initialDelay = "10s", fixedRate = "10s")
    fun getData() {
        try {
            val response = Unirest
                .get("https://opensky-network.org/api/states/all?lamin=35.9468&lomin=-9.392883&lamax=43.7483&lomax=3.03948")
                .asJson().body
            response.`object`.getJSONArray("states").forEach {
                val data = it as JSONArray
                val icao24 = data.getString(0)
                val callSign = data.getString(1)
                val originCountry = data.getString(2)
                val timePosition = data.getLong(3)

                val model = OpenSkyModel(
                    icao24,
                    callSign.trim(),
                    originCountry,
                    timePosition,
                    data.getFloatOrNull(5),
                    data.getFloatOrNull(6),
                    data.getFloatOrNull(7),
                    data.optBoolean(8),
                    data.getFloatOrNull(9),
                    data.getFloatOrNull(10),
                    data.getFloatOrNull(11),
                    data.getFloatOrNull(13),
                    data.optString(14)
                )
                val toJson = Gson().toJson(model)
                openSkyFeederQueue.send(toJson.toByteArray())
            }
        } catch (e: Exception) {
            println("Peto")
            e.printStackTrace()
        }
    }

    fun JSONArray.getFloatOrNull(index: Int): Float? {
        return try {
            this.getFloat(index)
        } catch (e: Exception) {
            null
        }
    }

    data class OpenSkyModel(
        private val icao24: String,
        private val callSign: String?,
        private val originCountry: String?,
        private val lastContactTimestamp: Long?,
        private val longitude: Float?,
        private val latitude: Float?,
        private val baroAltitude: Float?,   //meters
        private val onGround: Boolean?,
        private val velocity: Float?,
        private val track: Float?,
        private val verticalRate: Float?,
        private val geoAltitude: Float?,    //metes
        private val squawk: String?
    )

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            PicocliRunner.run(AdsbAntennaFeederCommand::class.java, *args)
        }
    }
}
