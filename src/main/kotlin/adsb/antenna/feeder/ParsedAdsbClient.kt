package adsb.antenna.feeder

import io.micronaut.rabbitmq.annotation.Binding
import io.micronaut.rabbitmq.annotation.RabbitClient

@RabbitClient
interface ParsedAdsbClient {

    @Binding("parsedadsb")
    fun send(data: ByteArray)
}
