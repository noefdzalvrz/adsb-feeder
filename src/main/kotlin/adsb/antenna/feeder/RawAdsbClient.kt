package adsb.antenna.feeder

import io.micronaut.rabbitmq.annotation.Binding
import io.micronaut.rabbitmq.annotation.RabbitClient

@RabbitClient
interface RawAdsbClient {

    @Binding("rawadsb")
    fun send(data: ByteArray)
}
