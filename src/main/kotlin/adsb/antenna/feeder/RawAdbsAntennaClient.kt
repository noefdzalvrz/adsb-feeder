package adsb.antenna.feeder

import io.reactivex.Flowable
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.Socket

class RawAdbsAntennaClient(
    addr: String,
    port: Int
) {
    private val socket: Socket = Socket(addr, port)
    private val input = BufferedReader(InputStreamReader(socket.getInputStream()))
    private val outputObserver = Flowable.fromIterable(Iterable { input.lines().iterator() })

    fun onNewMessage(action: (String) -> Unit): RawAdbsAntennaClient {
        outputObserver.forEach(action)
        return this
    }
}